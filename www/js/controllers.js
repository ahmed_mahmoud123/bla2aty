var controllerApp=angular.module('starter.controllers', ['ngCordova']);
function isNoConnection() {
  var networkState = navigator.connection.type;
  return networkState==Connection.NONE
};

function validateNumber(number){
  var numberRegex = /^05([0-9]{8})?$/;
  if(number.match(numberRegex)) {
    return true;
  }else {
    return false;
  }
}
// An alert dialog
function showAlert(title,subTitle,$ionicPopup,styleClass,message){
  $ionicPopup.show({
  title: '<span class="'+(styleClass==undefined?'error':styleClass)+'">'+title+'</span>',
  subTitle: '<span class="'+(styleClass==undefined?'error':styleClass)+'">'+subTitle+'</span>',
  buttons: [
  {
    text: '<b>'+(message==undefined?'إعادة المحاولة':message)+'<b>',
  }]
});
};
controllerApp.directive('ngEnter', function() {
        return function(scope, element, attrs) {
            element.bind("keydown keypress", function(event) {
                if(event.which === 13) {
                  scope.$apply(function(){
                    scope.$eval(attrs.ngEnter);
                  });
                  event.preventDefault();
                }
            });
        };
});

controllerApp.controller('AppCtrl', function($scope, $ionicModal, $timeout) {
})
controllerApp.controller('HomeCtrl', function($scope,$ionicHistory) {


  $ionicHistory.removeBackView();
  //$ionicHistory.clearHistory();
  //var deviceInformation = ionic.Platform.device();
  //console.log(JSON.stringify(deviceInformation));
})

controllerApp.controller('SuccessCtrl', function($scope,$stateParams) {
  $scope.id=$stateParams.id;
  $scope.restart=function(evt){
    evt.preventDefault();
    document.location.href = 'index.html';
  }
});

controllerApp.controller('UseContCtrl', function($scope, $stateParams) {
  $scope.response={
    type:2,
    contractor_id:$stateParams.id
  }
});
