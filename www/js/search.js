controllerApp.controller('SearchCtrl', function($scope,$http,$stateParams) {
    console.log($stateParams.id);
    var response=JSON.parse($stateParams.id);
    var type=response.type;
    $scope.type=type;
    var contractor=response.contractor_id;
    var custom_url;
    $scope.opened="active";
    var status=0;

    if(type==1)
    {
      custom_url="?Status=";
      sendRequest("?Status=0");
    }else if(contractor!=undefined)
    {
      custom_url="?contractor_id="+contractor+"&Status=";
      sendRequest(custom_url+status);
      console.log("contractor = "+contractor);
    }else{
      type=3;
      $scope.type=type;
      console.log(type);
      $scope.no_result="أدخل رقم الهاتف للبحث";
    }

    $scope.search=function(keyword){
      $scope.complaints=[];
      if(type!=3){
        if(keyword==undefined || keyword=='')
          sendRequest(custom_url+status);
        else
          sendRequest("/"+keyword+"?Status="+status);
      }else{
        sendRequest("?Mobile="+keyword+"&Status="+status);
      }
    }

    var mobile;
    $scope.emptySearch=function(keyword){
      if(keyword==""){
        $scope.complaints=[];
        $scope.no_result="أدخل رقم الهاتف";
      }
      mobile=keyword
    }

    $scope.setClose=function(evt){
      console.log("Type "+type);
      evt.preventDefault();
      $scope.opened="";
      status=1;
      $scope.closed="active";
      $scope.complaints=[];

      if(type!=3){
        sendRequest(custom_url+status);
      }else{
        if(mobile==undefined || mobile=='')return;
        sendRequest("?Mobile="+mobile+"&Status="+status);
      }
    }

    $scope.setOpen=function(evt){
      console.log("Type "+type);
      evt.preventDefault();
      status=0;
      $scope.closed="";
      $scope.opened="active";
      $scope.complaints=[];
      if(type!=3){
        sendRequest(custom_url+status);
      }else{
        if(mobile==undefined || mobile=='')return;
        sendRequest("?Mobile="+mobile+"&Status="+status);
      }
    }


    //GET api/Complaints?Mobile={Mobile}&Status={Status}
    function sendRequest(url){
      console.log('http://dammamlightinggis.com:2020/api/Complaints'+url);
      $scope.no_result="";
      $scope.loading=true;
      $http({
        method: 'GET',
        url: 'http://dammamlightinggis.com:2020/api/Complaints'+url,
        responseType:'json',
        headers: {
            'Content-type': 'application/json'
        },
      }).then(function successCallback(response) {
          $scope.loading=false;
          $scope.complaints=response.data;
          console.log(JSON.stringify (response.data));
          if(response.data.length==0){
            $scope.no_result="لا توجد نتائج";
            $scope.complaints=[];
          }
        }, function errorCallback(response) {
          $scope.loading=false;
          $scope.no_result="عملية بحث خاطئة";
          $scope.complaints=[];
        });
    }

})
