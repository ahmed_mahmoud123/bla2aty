controllerApp.controller('StaticCtrl', function($scope,$http) {
  //$scope.no_result="أدخل رقم المقاول للبحث";

  $http.get("http://dammamlightinggis.com:2020/api/Contractor")
  .then(function(response){
    $scope.options=response.data;
    $scope.options.push({id:0,full_name:"الكل"});
    var length=Number(response.data.length)-1;
    $scope.contractor_id=(response.data[length]).id;
    $scope.searchMo2awel((response.data[length]).id);
  },function(){

  });


  $scope.searchMo2awel=function(keyword){
    $scope.loading=true;
    $scope.data=[];
    $http.get("http://dammamlightinggis.com:2020/api/Complaints?ContractorID="+keyword)
    .then(function(response){
      console.log(JSON.stringify(response));
      $scope.loading=false;
      response=response.data[0];
      var percentage=Math.round((Number(response.TClosed)/(Number(response.TClosed)+Number(response.TOpen)))*100)
      $scope.data=[{
        open:response.TOpen,
        close:response.TClosed,
        name:response.full_name,
        count:Number(response.TOpen)+Number(response.TClosed),
        precentage:isNaN(percentage)?0:percentage,
        success_per:(100-percentage)
      }]
      console.log($scope.data[0].precentage+" %");
    },function(response){
      $scope.loading=false;
      $scope.no_result="خطأ";
      $scope.data=[];
    });
  }
});
