
controllerApp.controller('LoginCtrl', function($scope,$http,$cordovaNetwork,$ionicPopup,$location,$stateParams) {
  var type=$stateParams.id;
  var response={};
  var opt=type==1 ? "emp=":"Contractor=";
  console.log(type);
  $scope.login=function(username,password){
    if(isNoConnection()){
      showAlert("خطأ","أنت غير متصل بالأنترنت",$ionicPopup);
      return;
    }
    var url="http://dammamlightinggis.com:2020/api/Complaints";
    $scope.loading_login=true;
    console.log(url+"?"+opt+username+"&pass="+password);
    $http.get(url+"?"+opt+username+"&pass="+password)
    .then(function(resp){
      $scope.loading_login=false;
      console.log(JSON.stringify(resp));
      response.type=type;
      if(type==2){
        response.contractor_id=resp.data.contractor_id;
        $location.url("/app/use_cont/"+response.contractor_id);
      }else if(type==1){
        $location.url("/app/use_emp");
      }
    },function(resp){
      $scope.loading_login=false;
      showAlert("خطأ","بيانات الدخول خاطئة",$ionicPopup);
    });
  };
})
