controllerApp.controller('RequestCtrl', function($scope,$cordovaNetwork,$ionicPopup,$http,$cordovaCamera,$cordovaBarcodeScanner,$location) {


  var mobile;
  var cat_id=6;
  // get categories function
  function getCategories(){
    $http.get("http://dammamlightinggis.com:2020/api/Category")
    .then(function(response){
      $scope.categories=response.data;
      $scope.request={
        Mobile:mobile,
        CategoriesID:response.data[response.data.length-1].ID
      }
      console.log(" Length = "+$scope.categories.length+"  "+JSON.stringify(response));
    },function(response){
      console.log(JSON.stringify(response));
    })
  }
  getCategories();

  // check if complain exists before 48 hours function
  function is_complain_exist_before_48(pole,fader,panel,mobile,handler){
    $http({ContentType:"text/json",url:"http://dammamlightinggis.com:2020/api/Complaints?pole="+pole+"&fader="+fader+"&panel="+panel+"&mobile="+mobile})
    .then(function(response){
      console.log(JSON.stringify(response));
      handler(response);
    },function(response){
      console.log(JSON.stringify(response));
    })
  }

  $scope.setTel=function(keyword){
    mobile=keyword;
  }
  $scope.setCatId=function(value){
    cat_id=value;
    console.log(cat_id);
  }
  document.addEventListener("deviceready", function(){
    window.plugins.sim.getSimInfo(function(rs){
      $scope.request={
          Mobile:rs.phoneNumber
      };
      mobile=rs.phoneNumber;
    }, function(){});
  }, false);

  var pic=null;
  $scope.loading_request=false;
  // QR Code
  $scope.qr_code=function(){
    cloudSky.zBar.scan({}, function(rs){
      $scope.qr_icon="_green";
      var pos1 = rs.search("رقم المحطة");
      var pos2 = rs.search("الفيدر");
      var res = rs.substr(pos1,pos2-pos1+10);
      var parts=res.split(":");
      var panel = parts[1].match(/\d/g);
      panel = panel.join("");
      var pole = parts[2].match(/\d/g);
      pole = pole.join("");
      var fader = parts[3].match(/\d/g);
      fader = fader.join("");
      qrData=rs.replace(" هاتف الطوارئ : 940 – ","");
      $scope.request={
        "Mobile":mobile,
        "Complaint":qrData,
        "Panel":Number(panel),
        "Pole":Number(pole),
        "Fader":Number(fader),
        "CategoriesID":cat_id
      };
      $scope.$apply();
    },function(error){
      console.log(error);
    })
  }

  // Camera
  $scope.camera=function(){
    var options = {
        quality: 75,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.CAMERA,
        allowEdit: true,
        encodingType: Camera.EncodingType.JPEG,
        targetWidth: 300,
        targetHeight: 300,
        popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: false
    };
    $cordovaCamera.getPicture(options).then(function (imageData) {
      $scope.camera_icon="_green";
        pic = "data:image/jpeg;base64," + imageData;
        if (pic.split(',')[0].indexOf('base64') >= 0)
        {
          pic = pic.split(',')[1];
        }
        else
        {
          pic = unescape(pic.split(',')[1]);
        }
    }, function (err) {
      $scope.camera_icon="";
      pic=null;
    });
  }
  $scope.camera_icon="";
  $scope.qr_icon="";


  $scope.sendComplaint=function(request){
    if(isNoConnection()){
      showAlert("خطأ","أنت غير متصل بالأنترنت",$ionicPopup);
      return;
    }

    if(!validateNumber(request.Mobile)){
      showAlert("خطأ","الهاتف لابد انا يبدأ ٠٥ ولا يزيد عن ١٠ ارقام",$ionicPopup);
      return;
    }
    $scope.loading_request=true;
    is_complain_exist_before_48(request.Pole,request.Fader,request.Panel,request.Mobile
      ,function(response){
      if(response.data>0){
        $scope.loading_request=false;
        showAlert("خطأ","تم استلام بلاغ عن هذا العمود رقم - "+response.data,$ionicPopup);
        return;
      }else if(response.data==-1){
        $scope.loading_request=false;
        showAlert("خطأ","لا يوجد عمود بهذا الرقم",$ionicPopup);
        return;
      }
      var data={
        Mobile:request.Mobile,
        Pole:request.Pole,
        Panel:request.Panel,
        Fader:request.Fader,
        Complaint:request.Complaint,
        Pic:pic,
        CategoriesID:request.CategoriesID,
        ContentType: "image/png"
      }

      console.log(JSON.stringify(data));
      $http({
        method: 'POST',
        url: 'http://dammamlightinggis.com:2020/api/Complaints',
        data:JSON.stringify(data),
      }).then(function successCallback(response) {
          $scope.loading_request=false;
          if(response.statusText=="Created"){
            $scope.request={};
            pic=null;
            $scope.camera_icon="";
            $scope.qr_icon="";
            $location.url("/app/success/"+response.data.ID);
          }else{
            showAlert("خطأ","تعذر إرسال البلاغ",$ionicPopup);
          }
        }, function errorCallback(response) {
          $scope.loading_request=false;
          showAlert("خطأ","تعذر إرسال البلاغ",$ionicPopup);
        });
    });
  };
})
