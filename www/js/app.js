// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
var app=angular.module('starter', ['ionic', 'starter.controllers','ngCordova'])

app.run(function($ionicPlatform,$rootScope) {
  $rootScope.navTitle='<img class="title-image" src="img/logo.gif" />';
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

app.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.search', {
    url: '/search/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html',
        controller: 'SearchCtrl'
      }
    }
  })

  .state('app.use_emp', {
    url: '/use_emp',
    views: {
      'menuContent': {
        templateUrl: 'templates/use_emp.html',
      }
    }
  })

  .state('app.information', {
    url: '/information',
    views: {
      'menuContent': {
        templateUrl: 'templates/information.html',
      }
    }
  })

  .state('app.use_cont', {
    url: '/use_cont/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/use_cont.html',
        controller: 'UseContCtrl'

      }
    }
  })

  .state('app.static', {
    url: '/static',
    views: {
      'menuContent': {
        templateUrl: 'templates/static.html',
        controller: 'StaticCtrl'
      }
    }
  })
  .state('app.close_complain', {
    url: '/close_complain/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/close_complain.html',
        controller: 'ClossComplainCtrl'
      }
    }
  })
  .state('app.success', {
    url: '/success/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/success.html',
        controller: 'SuccessCtrl'
      }
    }
  })

  .state('app.use', {
      url: '/use',
      views: {
        'menuContent': {
          templateUrl: 'templates/use.html'
        }
      }
    })

    .state('app.display', {
        url: '/display',
        views: {
          'menuContent': {
            templateUrl: 'templates/display.html'
          }
        }
      })

    .state('app.login', {
        url: '/login/:id',
        views: {
          'menuContent': {
            templateUrl: 'templates/login.html',
            controller: 'LoginCtrl'
          }
        }
      })

    .state('app.home', {
      url: '/home',
      views: {
        'menuContent': {
          templateUrl: 'templates/home.html',
          controller: 'HomeCtrl'
        }
      }
    })

  .state('app.request', {
    url: '/request',
    views: {
      'menuContent': {
        templateUrl: 'templates/request.html',
        controller: 'RequestCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/home');
});
