controllerApp.controller('ClossComplainCtrl', function($cordovaCamera,$stateParams,$scope,$http,$ionicPopup) {
  //$scope.id=$stateParams.id;
  $scope.camera_icon="";
  var pic=null;
  $scope.closeComplaint=function(request){
    $scope.loading_request=true;
    if(isNoConnection()){
      showAlert("خطأ","أنت غير متصل بالأنترنت",$ionicPopup);
      return;
    }
    var data={
      ComplaintID:request.ID,
      Report:request.Report,
      Pic:pic,
      CreateDate:null,
      ID:"",
      ContentType: "image/png"
    }
    console.log(JSON.stringify(data));

    $http({
      method: 'POST',
      url: 'http://dammamlightinggis.com:2020/api/ComplaintReport',
      data:JSON.stringify(data),
    }).then(function successCallback(response) {
        $scope.loading_request=false;
        console.log(JSON.stringify(response));
        showAlert("تم","تم اغلاق البلاغ بنجاح",$ionicPopup,'done','حسناً');
      }, function errorCallback(response) {
        $scope.loading_request=false;
        console.log(JSON.stringify(response));
        if(response.data.Message == 0){
          showAlert("تعذر","لا يوجد بلاغ بهذا الرقم او ربما اغلق من قبل",$ionicPopup);
        }
      });

  }



  $scope.camera=function(){
    var options = {
        quality: 75,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.CAMERA,
        allowEdit: true,
        encodingType: Camera.EncodingType.JPEG,
        targetWidth: 300,
        targetHeight: 300,
        popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: false
    };
    $cordovaCamera.getPicture(options).then(function (imageData) {
      $scope.camera_icon="_green";
        pic = "data:image/jpeg;base64," + imageData;
        if (pic.split(',')[0].indexOf('base64') >= 0)
        {
          pic = pic.split(',')[1];
        }
        else
        {
          pic = unescape(pic.split(',')[1]);
        }
    }, function (err) {
      $scope.camera_icon="";
      pic=null;
    });
  }

});
